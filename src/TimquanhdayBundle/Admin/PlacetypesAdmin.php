<?php

namespace TimquanhdayBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class PlacetypesAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $thumbnail = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($thumbnail && ($webPath = $thumbnail->getThumbnail())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$webPath;
            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" width="50" />';
        }

        $formMapper
            ->add('name')
            ->add('description')
            ->add('keyword')
            ->add('file', 'file', $fileFieldOptions)
            ->end()
        ;
    }

    public function prePersist($placetypes) 
    {
        $this->saveFile($placetypes);
    }

    public function preUpdate($placetypes) 
    {
        $this->saveFile($placetypes);
    }

    public function saveFile($placetypes) 
    {
        $basepath = $this->getRequest()->getBasePath();
        $placetypes->upload($basepath);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('description')
            ->add('keyword')
            ->add('thumbnail')
        ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('name')
                ->assertLength(array('max' => 32))
            ->end()
        ;
    }
}