<?php

namespace TimquanhdayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="_home")
     */
    public function indexAction()
    {
        $placeTypes = $this->getDoctrine()
                    ->getRepository('TimquanhdayBundle:PlaceTypes')
                    ->findAll();

        return $this->render('TimquanhdayBundle:Default:index.html.twig', array(
                'placeTypes' => $placeTypes
            ));
    }

    /**
     * @Route("/show", name="_show")
     */
    public function showAction(Request $request)
    {
        $listPlaceTypes = $this->getDoctrine()
                        ->getRepository('TimquanhdayBundle:PlaceTypes')
                        ->findAll();
        $keyword = $request->query->get('place_type');
        $radius = $request->query->get('radius');
        $placeType = $this->getDoctrine()
                        ->getRepository('TimquanhdayBundle:PlaceTypes')
                        ->findOneBy(array('keyword'=>$keyword));
        $radiuses = array(
                '500' => '500m',
                '1000' => '1km',
                '1500' => '1,5km',
                '2000' => '2km',
                '2500' => '2,5km'
            );

        if (hash_hmac('sha256', $keyword.$radius, $this->container->getParameter('secret')) === $request->query->get('hash')) {

            return $this->render('TimquanhdayBundle:Default:show.html.twig', array(
                'listPlaceTypes' => $listPlaceTypes,
                'keyword' => $keyword,
                'placeTypeName' => $placeType->getName(),
                'radius'     => $radius,
                'radiuses'  => $radiuses
            ));
        }

        return $this->redirect($this->generateUrl('_404'));
    }

    /**
     * @Route("/progress", name="_progress")
     */
    public function progressAction(Request $request)
    {
        if(!$request->request->get('place_type') or !$request->request->get('radius')) {
            return $this->redirect($this->generateUrl('_404'));
        }

        // $placeTypes = $request->query->get('place_types');
        // $radius = $request->query->get('radius');

        $placeType = $request->request->get('place_type');
        $radius = $request->request->get('radius');
        // $placeType = $this->getDoctrine()
        //                 ->getRepository('TimquanhdayBundle:PlaceTypes')
        //                 ->findOneBy(array('keyword'=>$placeTypes[0]));

        $hashString = hash_hmac('sha256', $placeType.$radius, $this->container->getParameter('secret'));

        return $this->redirect($this->generateUrl('_show', array('place_type' => $placeType,
'radius' => $radius, 'hash' => $hashString)));
    }
}
