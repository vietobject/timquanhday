( function( $ ) {
	$(document).ready(function(){

		$('button.close').click(function(e){
			$(this).parent().hide();
			$('#right-pannel-info-expandbtn').show();
		});

		$('#right-pannel-info-expandbtn').click(function(e){
			$(this).hide();
			$('#right-pannel-info').show();
		});

		$('#reset').click(function(e){
			$('#right-pannel-info').hide();
			$('#right-pannel-info-expandbtn').hide();
		});

		$('input.search-box').val('');

		var totalPlace = 0;
		$('#place-types .type .img-thumbnail').click(function(e){
			e.preventDefault();

			$('#place-types .type .img-thumbnail').each(function(){
				if(rgb2hex($(this).css('border-top-color')) == '#0088cc') {
					totalPlace++;
				}
			});

			if(rgb2hex($(this).css('border-top-color')) != '#0088cc') {
				if(totalPlace < 1) {
					$(this).css('border-color', '#0088cc');	
					totalPlace++;
				} else {
					$('#place-types .type .img-thumbnail').css('border-color', '#ddd');
					$(this).css('border-color', '#0088cc');	
				}
			}
			else {
				$(this).css('border-color', '#ddd');
				totalPlace--;
			}
		});

		$('#place-types .modal-footer .btn-primary').click(function(e){
			//e.preventDefault();
			var placeType;
			var keyword;

			$('#place-types .type .img-thumbnail').each(function(){
				if(rgb2hex($(this).css('border-top-color')) == '#0088cc') {
					placeType = $(this).find('span.description').text();
					keyword = $(this).find('span.keyword').text();
				}
			});

			$('input.keyword').remove();
			$('<input type="hidden" class="keyword" name="place_type" value="'+ keyword +'" />').insertAfter('.search-form .search-box');

			$('input.search-box').val("Tìm vị trí: " + placeType);
			// $('input.keyword').val(keyword);
		});

		$('form.search-form').submit(function(e){
			if($('select.search-dropdown option:selected').val() < 1) {
				e.preventDefault();
				var selectBoxWidth = $('select.search-dropdown').outerWidth();

				$('form.search-form').prepend('<div style="opacity: 1; margin-left: -'+ selectBoxWidth +'px" class="tooltip left" role="tooltip"> <div class="tooltip-arrow"></div> <div class="tooltip-inner"> Bạn chưa chọn bán kính </div> </div>');
				$('select.search-dropdown').click(function(e){
					$('form.search-form').find('.tooltip.left').remove();
				})
			}

			if($('input.search-box').val() == '') {
				e.preventDefault();
				var searchBoxHeight = $('input.search-box').outerHeight();
				var selectBoxWidth = $('select.search-dropdown').outerWidth();

				$('<div style="opacity: 1; margin-top: -'+ (searchBoxHeight/2) +'px; margin-left: '+ selectBoxWidth +'px" class="tooltip top" role="tooltip"> <div class="tooltip-arrow"></div> <div class="tooltip-inner"> Bạn chưa chọn loại địa điểm </div> </div>').insertBefore('input.search-box');
				$('input.search-box').click(function(e){
					$('form.search-form').find('.tooltip.top').remove();
				})
			}
		});

	});

} )( jQuery );

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

//Function to convert hex format to a rgb color
function rgb2hex(rgb) {
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
	return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
